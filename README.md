Tugas Rumah Tutorial GIT

Clone project ini
Buat branch baru dengan nama tugas-nama. Contoh: tugas-eka

Edit file Tugas Rumah GIT.txt dengan menjelaskan istilah istilah git yang sudah disediakan dengan bahasa sendiri
Tugas Rumah menggunakan bahasa Indonesia

Commit perubahan file dengan pesan "nama - edit Tugas Rumah GIT". Contoh: "Eka - edit Tugas Rumah GIT"
Untuk pengumpulan menggunakan fitur merge request ke branch master

Pengumpulan paling lambat Kamis, 5 November 2020 10.00 WIB



Jika ada pertanyaan bisa melalui chanel slack #magang-smkn13-2020 dan tag ekapratama, Dzikri Azzakah Asshiam, atau Fahrul Fauzi


Refferences

Checkout new branch

$ git checkout -b *Name*

Commit changes

$ git commit -m "*Message*"

Full cheatsheet